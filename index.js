var shjs = require('shelljs');
    dive = require('dive'),
    fs = require('fs');

    //Shell Commands
var makeFolder = 'mkdir -p output',
    moveCmd =  'mv *.gif output/';

console.log('Creating Gif\'s...');

dive('tracks', {  recursive: false, all: false, directories: true, files: false}, function(err, file) {

    if (err) throw err; // Throw error if cannot dive through tracks foldernode

    fs.readdir(file, function (err, files) {

        var overlayPath = file + '/overlays';
        //console.log(overlayPath)

        fs.readdir(overlayPath, function (err, overlays) {

            //console.log(overlays); // All Overlays for Base .gif
            var i;

            for (i in files) {

                if( files[i] !== 'overlays' ){

                    var k;
                    for (j in overlays){

                        if(overlays[j] !== '.DS_Store'){
                            //console.log(overlays[j]); //Current Overlay .png
                            //console.log(files[i]); //current Base .gif

                            //setup dynamic gif input/output varibles
                        var trackGif = file + '/' + files[i],
                            lyricOverlay = file + '/overlays/' + overlays[j];
                            nameLessExt = files[i].split(".");
                            outputName =  nameLessExt[0] + '-' + overlays[j] + '.gif';

                            //Create Shell commands
                            convertCmd = 'convert ' + trackGif + ' -coalesce -gravity Center -geometry +0+0 null: ' + lyricOverlay + ' -layers composite -layers optimize ' + outputName + '' ;

                            console.log("Applying Overlay :", overlays[j] , " ---> ", files[i]);

                            //Run Shell commands
                            shjs.exec(convertCmd);
                            shjs.exec(makeFolder);
                            shjs.exec(moveCmd);

                        }//If not .DS store ~

                    }//for each .png in Overlays folder

                }// If not overlays folder

            }//for contents of tracks folder

        });// Reaing Overlay folder

    });// Read base .gifs in each  folder

});//Read all tracks in track folder
